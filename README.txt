ABOUT
-----
This module provides Paguelo Facil (http://www.paguelofacil.com/) integration for the
Payment platform (https://drupal.org/project/payment). Paguel Facil is a Panamanian payment service provider.
With this module the site owner will be available to enable customers to pay with this type of payment.

Using the paguelofacil payment module you will be able to use the "PagueloFacil botón de pago" which is a similar
to paypal's button method.

INSTALLATION AND CONFIGURATION
------------------------------
This module follows the standard methods for payment payments, you can find information about it in
https://www.drupal.org/node/2320495

